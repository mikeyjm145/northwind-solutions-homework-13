﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Suppliers.aspx.cs" Inherits="Suppliers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 13: Northwind</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            width: 271px;
        }
        .auto-style2 {
            width: 280px;
        }
    </style>
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
        <form id="form1" runat="server">
            <label>Choose a country:&nbsp;</label>
            <asp:DropDownList ID="ddlCategory" runat="server" DataSourceID="sqldsSuppliers" DataTextField="Country" DataValueField="Country" AutoPostBack="True"></asp:DropDownList>
            <asp:SqlDataSource ID="sqldsSuppliers" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionStringSupplier %>" ProviderName="<%$ ConnectionStrings:ConnectionStringSupplier.ProviderName %>" SelectCommand="SELECT DISTINCT [Country] FROM [tblSuppliers] ORDER BY [Country]"></asp:SqlDataSource>
            <section>
                <asp:DataList ID="dlProducts" runat="server" DataSourceID="SqlDataSource1">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td class="auto-style1">Company Name</td>
                                <td class="auto-style2">Phone Number</td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td class="auto-style1">
                                    <asp:Label ID="lblID" runat="server" 
                                        Text='<%# Eval("CompanyName") %>' />
                                </td>
                                <td class="col2">
                                    <asp:Label ID="lblName" runat="server" 
                                        Text='<%# Eval("Phone") %>' />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <AlternatingItemStyle BackColor="#669999" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                </asp:DataList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" ProviderName="<%$ ConnectionStrings:ConnectionString2.ProviderName %>" SelectCommand="SELECT CompanyName, Phone FROM tblSuppliers WHERE (Country = ?)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlCategory" DefaultValue="" Name="?" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </section>
        </form>
    </section>
</body>
</html>
